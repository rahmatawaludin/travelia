<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Room;
use App\Facility;
use App\Review;

class Hotel extends Model
{
    protected $fillable = ['name', 'city', 'address', 'country', 'phone', 
        'email', 'website', 'latitude', 'longitude'];

    public function rooms()
    {
        return $this->hasMany(Room::class);
    }

    public function facilities()
    {
        return $this->belongsToMany(Facility::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }
}

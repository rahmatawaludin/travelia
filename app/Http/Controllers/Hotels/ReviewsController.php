<?php

namespace App\Http\Controllers\Hotels;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Hotel;

class ReviewsController extends Controller
{
    public function index($hotel_id)
    {
        try {
            $hotel = Hotel::findOrFail($hotel_id);
            return $hotel->reviews()->paginate(10);
        } catch (ModelNotFoundException $e) {
            return response()->json(null, 404);
        }
    }
}

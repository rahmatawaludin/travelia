<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests;
use App\Hotel;
use App\Facility;

class HotelsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validSort = ['name', 'city', 'country'];
        $sort = 'name';
        if ($request->has('sort') && in_array($request->get('sort'), $validSort) ) {
            $sort = $request->get('sort');
        }
        $order = 'asc';
        if ($request->has('order') && in_array($request->get('order'), ['desc', 'asc']) ) {
            $order = $request->get('order');
        }

        if ($request->has('q')) {
            $q = $request->get('q');
            $hotels = Hotel::where('name', 'LIKE', '%'. $q . '%')
                ->orWhere('city', 'LIKE', '%'. $q . '%')
                ->orWhere('address', 'LIKE', '%'. $q . '%')
                ->orWhere('country', 'LIKE', '%'. $q . '%')
                ->orderBy($sort, $order)
                ->paginate(5)->appends(compact('q', 'sort', 'order'));
        } else {
            $hotels = Hotel::orderBy($sort, $order)
                ->paginate(5)
                ->appends(compact('sort', 'order'));
        }

        $response = [
            'total' => $hotels->total(),
            'per_page' => $hotels->perPage(),
            'current_page' => $hotels->currentPage(),
            'last_page' => $hotels->lastPage(),
            'next_page_url' => $hotels->nextPageUrl(),
            'prev_page_url' => $hotels->previousPageUrl(),
            'from' => $hotels->firstItem(),
            'to' => $hotels->lastItem(),
            'data' => []
        ];

        // membuat struktur `data` sesuai JSON API
        foreach ($hotels->items() as $item) {
            $hotel = [
                'id' => $item->id,
                'type' => 'hotels',
                'attributes' => [
                    'name'      => $item->name,
                    'city'      => $item->city,
                    'address'   => $item->address,
                    'country'   => $item->country,
                    'phone'     => $item->phone,
                    'email'     => $item->email,
                    'website'   => $item->website,
                    'latitude'  => $item->latitude,
                    'longitude' => $item->longitude
                ]
            ];
            array_push($response['data'], $hotel);
        }

        return response()->json($hotels, 200, [
            'Content-Language' => config('app.locale'),
            'X-Response-Time' => microtime(true) - LARAVEL_START
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->input('data.attributes', []), [
            'name'      => 'required|unique:hotels',
            'city'      => 'required',
            'address'   => 'required',
            'country'   => 'required',
            'phone'     => 'required',
            'email'     => 'required|email',
            'website'   => 'required|url',
            'latitude'  => 'required|numeric',
            'longitude' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->validationError($validator);
        }

        $facility_id = collect($request->input('data.relationships.facilities.data', []))->pluck('id');
        // filter only valid id
        $facility_id = Facility::whereIn('id', $facility_id)->pluck('id')->toArray();

        $hotel = Hotel::create($request->input('data.attributes'));

        // save facilities
        $hotel->facilities()->sync($facility_id);

        $response = ['data' => [
            'id'   => $hotel->id,
            'type' => 'hotels',
            'attributes' => [
                'name'      => $hotel->name,
                'city'      => $hotel->city,
                'address'   => $hotel->address,
                'country'   => $hotel->country,
                'phone'     => $hotel->phone,
                'email'     => $hotel->email,
                'website'   => $hotel->website,
                'latitude'  => $hotel->latitude,
                'longitude' => $hotel->longitude
            ]
        ]];

        return response()->json($response, 201)
            ->header('Location', $request->root() . '/hotels/' . $hotel->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $hotel = Hotel::findOrFail($id);
            $response = [
                'data' => [
                        'id'   => $hotel->id,
                        'type' => 'hotels',
                    'attributes' => [
                        'name'      => $hotel->name,
                        'city'      => $hotel->city,
                        'address'   => $hotel->address,
                        'country'   => $hotel->country,
                        'phone'     => $hotel->phone,
                        'email'     => $hotel->email,
                        'website'   => $hotel->website,
                        'latitude'  => $hotel->latitude,
                        'longitude' => $hotel->longitude
                    ]
                ], 
                // 'links' => [
                //     'reviews' => $hotel->reviews()->pluck('id'),
                //     'rooms' => $hotel->rooms()->pluck('id')
                // ]
                'relationships' => [
                    'reviews' => [
                        'links' => [
                            'self' => request()->root() . '/reviews',
                            'related' => request()->root() . '/hotels/' . $id . '/reviews'
                        ],
                        'data' => $hotel->reviews->map(function($item, $key) {
                            return [
                                'type' => 'reviews',
                                'id' => $item->id
                            ];
                        })->toArray()
                    ],
                    'rooms' => [
                        'links' => [
                            'self' => request()->root() . '/rooms',
                            'related' => request()->root() . '/hotels/' . $id . '/rooms'
                        ],
                        'data' => $hotel->rooms->map(function($item, $key) {
                            return [
                                'type' => 'rooms',
                                'id' => $item->id
                            ];
                        })->toArray()
                    ]
                ]
            ];

            return response()->json($response)
                ->header('Content-Language', config('app.locale'))
                ->header('X-Response-Time', microtime(true) - LARAVEL_START);
        } catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $hotel = Hotel::findOrFail($id);
            $rules = collect([
                'name'      => 'required|unique:hotels',
                'city'      => 'required',
                'address'   => 'required',
                'country'   => 'required',
                'phone'     => 'required',
                'email'     => 'required|email',
                'website'   => 'required|url',
                'latitude'  => 'required|numeric',
                'longitude' => 'required|numeric',
                'name'      => 'required|unique:hotels,name,' . $id,
                'address'   => 'required',
            ]);

            if ($request->isMethod('patch')) {
                $rules = $rules->map(function($rule, $field) {
                    return 'sometimes|' . $rule;
                });
            }

            $validator = Validator::make($request->input('data.attributes', []), $rules->toArray());

            if ($validator->fails()) {
                return response()->validationError($validator);
            }

            $hotel->update($request->input('data.attributes', []));

            $response = ['data' => [
                'id'   => $hotel->id,
                'type' => 'hotels',
                'attributes' => [
                    'name'      => $hotel->name,
                    'city'      => $hotel->city,
                    'address'   => $hotel->address,
                    'country'   => $hotel->country,
                    'phone'     => $hotel->phone,
                    'email'     => $hotel->email,
                    'website'   => $hotel->website,
                    'latitude'  => $hotel->latitude,
                    'longitude' => $hotel->longitude
                ]
            ]];

            return response()->json($response);
        } catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Hotel::where('id', $id)->delete();
        return response()->json(null, 204);
    }

    public function reviews($hotel_id)
    {
        try {
            $hotel = Hotel::findOrFail($hotel_id);
            return $hotel->reviews()->paginate(10);
        } catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Transformers\ReviewTransformer;
use App\Hotel;
use App\Review;
use App\User;
use Illuminate\Support\Facades\Validator;

class ReviewsController extends Controller
{
    public function index(ReviewTransformer $transformer, Request $request)
    {
        $reviews = Review::paginate(5);
        if ($request->has('include')) {
            $transformer = $transformer->parseInclude($request->get('include'));
        }
        $response = $transformer->parseFields($request->get('fields'))
            ->transformCollection($reviews);
        return response()->json($response);
    }

    public function show(ReviewTransformer $transformer, Request $request, $id)
    {
        try {
            $review = Review::findOrFail($id);
            if ($request->has('include')) {
                $transformer = $transformer->parseInclude($request->get('include'));
            }
            $response = $transformer->parseFields($request->get('fields'))
                ->transformItem($review, 'data');
            return response()->json($response);
        } catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    public function store(Request $request)
    {
        $data             = $request->input('data.attributes', []);
        $data['user_id']  = $request->input('data.relationships.author.data.id', null);
        $data['hotel_id'] = $request->input('data.relationships.hotel.data.id', null);

        $validator = Validator::make($data, [
            'comment'  => 'required',
            'rating'   => 'required|numeric|between:0,10',
            'user_id'  => 'required|exists:users,id',
            'hotel_id' => 'required|exists:hotels,id'
        ]);

        if ($validator->fails()) {
            return response()->validationError($validator);
        }

        $review   = Review::create($data);
        $response = (new ReviewTransformer)->parseInclude(['hotel', 'author'])->transform($review);

        return response()->json($response, 201)
            ->header('Location', $request->root() . '/reviews/' . $review->id);
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Validation\Validator;
use Response;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('validationError', function(Validator $validator) {
            $errors = [];
            $response = ['errors' => []];

            foreach ($validator->errors()->messages() as $field => $messages) {
                $errors[$field]['type'] = 'ValidationError';
                $errors[$field]['status'] = '422';
                $errors[$field]['detail'] = $messages[0];
            }

            foreach ($validator->failed() as $field => $rule) {
                $rule = key($rule);
                $errors[$field]['code'] = strtoupper('err-validation-' . $rule);
                $errors[$field]['links'] = [
                    'about' => 'http://dev.travelia.com/docs/errors#'.strtoupper('validation-' . $rule)
                ];
                $errors[$field]['source'] = ['pointer' =>'/data/attributes/'. $field];
                array_push($response['errors'], $errors[$field]);
            }

            return Response::json($response, 422);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Hotel;
use App\User;
use App\Vote;

class Review extends Model
{
    protected $fillable = ['comment', 'rating', 'user_id', 'hotel_id'];

    public static function boot()
    {
        parent::boot();

        static::saved(function ($review) {
            $hotel                 = $review->hotel;
            $total_reviewer        = $hotel->reviews()->count();
            $hotel->total_rating   = $hotel->reviews()->sum('rating') / $total_reviewer;
            $hotel->total_reviewer = $total_reviewer;
            $hotel->save();
        });
    }

    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function votes()
    {
        return $this->hasMany(Vote::class);
    }
}

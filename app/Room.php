<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Hotel;

class Room extends Model
{
    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }
}

<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

abstract class BaseTransformer {

    /** 
     * List allowed relationship to include.
     *
     * @var array
     */
    protected $possibleRelationships = [];

    /** 
     * List relationship being loaded.
     *
     * @var array
     */
    protected $loadedRelationships   = [];

    /** 
     * List fields to load for each type.
     *
     * @var array
     */
    protected $fields = [];

    /**
     * Transform model into formatted array without namespace
     * @param Model @model
     * @return array
     */
    abstract protected function transform(Model $model);

    /**
     * Transform model into formatted array
     * @param Model $model
     * @param null|string $namespace optional namespace
     * @return array
     */
    final public function transformItem(Model $model, $namespace = null)
    {
        $output = $this->transform($model);

        if (count($this->fields)) {
            $output['attributes'] = $this->filterAttributes($output['attributes'], $output['type']);
        }

        if ($namespace != null) {
            $output = [$namespace => $output];
        }

        if (count($this->loadedRelationships)) {
            $output['relationships'] = $this->generateRelationships($model);
            $output['included'] = $this->generateEmbeds($model);
        }

        return $output;
    }

    /**
     * Transform Paginated Eloquent Collection to formatted array
     * @param \Illuminate\Pagination\LengthAwarePaginator $models
     * @param null|string $namespace optional namespace
     * @return array
     */
    final public function transformCollection(Paginator $models, $namespace = 'data')
    {
        $output = [
            'total'         => $models->total(),
            'per_page'      => $models->perPage(),
            'current_page'  => $models->currentPage(),
            'last_page'     => $models->lastPage(),
            'next_page_url' => $models->nextPageUrl(),
            'prev_page_url' => $models->previousPageUrl(),
            'from'          => $models->firstItem(),
            'to'            => $models->lastItem()
        ];

        $items    = [];
        $included = collect();

        foreach($models as $model) {
            $item = $this->transform($model);

            if (count($this->fields)) {
                $item['attributes'] = $this->filterAttributes($item['attributes'], $item['type']);
            }

            if (count($this->loadedRelationships)) {
                $item['relationship'] = $this->generateRelationships($model);
            }

            array_push($items, $item);

            $embeds = $this->generateEmbeds($model);
            $included = $included->merge($embeds);
        }

        $output[$namespace] = $items;

        if (count($this->loadedRelationships)) {
            $output['included'] = $included->unique()->sortBy('id')->values();
        }

        return $output;
    }

    /**
     * Parse given include
     * @param mixed @includes 
     * @return $this
     */
    final public function parseInclude($includes)
    {
        // allow for parameter as string with comma or array
        $includes = is_string($includes) ? explode(',', $includes) : $includes;
        $possibleRelationships     = $this->possibleRelationships;
        $this->loadedRelationships = array_values(array_intersect($includes, $possibleRelationships));

        return $this;
    }

    /**
     * Generate array for `relationships` key in output
     * @param Model $model
     * @return array
     */
    final protected function generateRelationships(Model $model)
    {
        $relationships = [];
        foreach ($this->loadedRelationships as $relationship) {
            // subclass must create `xxxRelationship` method based on available relationship name
            $method = strtolower($relationship) . 'Relationship';
            $relationships[$relationship] = $this->$method($model);
        }

        return $relationships;
    }

    /**
     * Generate array for `included` key in output
     * @param Model $model
     * @return array
     */
    final protected function generateEmbeds(Model $model)
    {
        $embeds = collect();
        foreach($this->loadedRelationships as $relationship) {
            // subclass must create `embedXxx` method based on available relationship name
            $method = 'embed' . ucfirst($relationship);
            $embeds = $embeds->merge($this->$method($model));
        }

        return $embeds->unique()->sortBy('id')->values();
    }

    /**
     * Parse sparse fieldset parameter
     * @param mixed $fields
     * @return $this
     */
    public function parseFields($fields)
    {
        if (is_array($fields)) {
            $this->fields = collect($fields)->map(function($value, $key) {
                // change comma separated field name to array
                return explode(',', $value);
            })->toArray();
        }

        return $this;
    }

    /**
     * Filter output attribute by sparse fieldset
     * @param array $attributes
     * @param string $type
     * @return array
     */
    protected function filterAttributes($attributes, $type)
    {
        if (isset($this->fields[$type])) {
            // get allowed fields
            $allowedFields = collect($this->fields[$type]);
            // remove attribute not exists in `allowedFields`
            $attributes = collect($attributes)->filter(function($value, $key) use ($allowedFields) {
                return $allowedFields->contains($key);
            })->toArray();
        }

        return $attributes;
    }
}


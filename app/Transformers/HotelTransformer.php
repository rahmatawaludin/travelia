<?php

namespace App\Transformers;
use Illuminate\Database\Eloquent\Model;

class HotelTransformer extends BaseTransformer {

    protected function transform(Model $hotel)
    {
        return [
            'id'   => $hotel->id,
            'type' => 'hotels',
            'attributes' => [
                'name'      => $hotel->name,
                'city'      => $hotel->city,
                'address'   => $hotel->address,
                'country'   => $hotel->country,
                'phone'     => $hotel->phone,
                'email'     => $hotel->email,
                'website'   => $hotel->website,
                'latitude'  => $hotel->latitude,
                'longitude' => $hotel->longitude
            ]
        ];
    }
}

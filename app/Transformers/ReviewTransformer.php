<?php

namespace App\Transformers;
use App\Review;
use Illuminate\Database\Eloquent\Model;

class ReviewTransformer extends BaseTransformer {

    protected $possibleRelationships = ['author', 'votes', 'hotel'];

    protected function transform(Model $review)
    {
        return [
            'id'   => $review->id,
            'type' => 'reviews',
            'attributes' => [
                'comment'    => $review->comment,
                'rating'     => $review->rating,
                'created_at' => (string) $review->created_at,
                'updated_at' => (string) $review->updated_at,
            ]
        ];
    }

    protected function authorRelationship(Review $review)
    {
        return [
            'links' => [
                'self' => config('app.url'). '/users',
                'related' => config('app.url') . '/reviews/' . $review->id . '/votes'
            ],
            'data' => [
                'type' => 'users',
                'id'   => $review->user->id
            ]
        ];
    }

    protected function votesRelationship(Review $review)
    {
        return [
            'links' => [
                'self'    => config('app.url') . '/users',
                'related' => config('app.url') . '/reviews/' . $review->id . '/votes'
            ],
            'data' => $review->votes->map(function($vote, $key) {
                return [
                    'type' => 'users',
                    'id'   => $vote->user_id
                ];
            })->toArray()
        ];
    }

    protected function embedAuthor(Review $review)
    {
        $user = $review->user;
        return [ (new UserTransformer)->transformItem($user) ];
    }

    protected function embedVotes(Review $review)
    {
        $userTransformer = new UserTransformer;
        return $review->votes()->with('user')->get()
            ->map(function($vote, $key) use ($userTransformer){
                return $userTransformer->transformItem($vote->user);
            });
    }

    protected function hotelRelationship(Review $review)
    {
        return [
            'links' => [
                'self'    => config('app.url') . '/hotels',
                'related' => config('app.url') . '/reviews/' . $review->id . '/hotel'
            ],
            'data' => [
                'type' => 'hotels',
                'id'   => $review->hotel_id
            ]
        ];
    }

    protected function embedHotel(Review $review)
    {
        $hotel = $review->hotel;
        return [ (new HotelTransformer)->transformItem($hotel) ];
    }
}

<?php

namespace App\Transformers;
use Illuminate\Database\Eloquent\Model;

class UserTransformer extends BaseTransformer {

  protected function transform(Model $user)
  {
    return [
      'type' => 'users',
      'id'   => $user->id,
      'attributes' => [
        'name' => $user->name
      ]
    ];
  }
}


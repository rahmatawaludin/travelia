<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Review;

class Vote extends Model
{
    public $incrementing = false;
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function review()
    {
        return $this->belongsTo(Review::class);
    }
}

<?php
use App\User;
use App\Hotel;
use App\Review;
use App\Vote;

$factory->define(User::class, function (Faker\Generator $faker) {
  return [
    'name'           => $faker->name,
    'email'          => $faker->safeEmail,
    'password'       => bcrypt('secret'),
    'remember_token' => str_random(10),
  ];
});

$factory->defineAs(User::class, 'admin', function ($faker) use ($factory) {
  $user = $factory->raw(User::class);
  return array_merge($user, ['role' => 'admin']);
});

$factory->defineAs(User::class, 'member', function ($faker) use ($factory) {
  $user = $factory->raw(User::class);
  return array_merge($user, ['role' => 'member']);
});

$factory->define(Hotel::class, function (Faker\Generator $faker) {
  $website = $faker->domainName;
  return [
    'name'      => $faker->streetName . ' Hotel',
    'city'      => $faker->city,
    'address'   => $faker->streetAddress,
    'country'   => $faker->country,
    'phone'     => $faker->phoneNumber,
    'email'     => $faker->userName . '@' . $website,
    'website'   => $website,
    'latitude'  => $faker->latitude,
    'longitude' => $faker->longitude
  ];
});

$factory->define(Review::class, function (Faker\Generator $faker) {
  $hotel_id = Hotel::all()->random()->id;
  // only allow 1 review per user per hotel
  $blockUser = Review::where('hotel_id', $hotel_id)->pluck('user_id');
  $user_id  = User::member()->whereNotIn('id', $blockUser)->get()->random()->id;

  return [
    'user_id'  => $user_id,
    'hotel_id' => $hotel_id,
    'comment'  => $faker->text,
    'rating'   => $faker->randomFloat(1, 1, 10)
  ];
});

$factory->define(Vote::class, function (Faker\Generator $faker) {
  $review = Review::all()->random();

  // prevent vote twice on same review
  $blockUser = Vote::where('review_id', $review->id)->pluck('user_id')->toArray();
  // prevent vote for himself
  array_push($blockUser, $review->user_id);

  $user = User::member()->whereNotIn('id', $blockUser)->get()->random();

  return [
    'user_id'   => $user->id,
    'review_id' => $review->id
  ];
});

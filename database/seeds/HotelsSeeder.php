<?php

use Illuminate\Database\Seeder;
use App\Facility;
use App\Hotel;
use App\Review;
use App\Room;
use App\Vote;

class HotelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        // Sample hotels
        $hotels = factory(Hotel::class, 50)->create();

        // Sample facilities
        Facility::create(['name' => 'have_pool', 'description' => 'Have 1 or more swimming pool']);
        Facility::create(['name' => 'have_spa', 'description' => 'Ondemand Spa']);
        Facility::create(['name' => 'have_wifi', 'description' => 'Have a paid / free wifi']);
        Facility::create(['name' => 'have_breakfast', 'description' => 'Breakfast included']);

        Facility::get()->each(function($facility) use ($hotels, $faker) {
            $hotels->filter(function($hotel) use ($faker) {
                return $faker->boolean;
            })->each(function($hotel) use ($facility) {
                $hotel->facilities()->save($facility);
            });
        });

        // Sample rooms
        $specs = collect([
            ['Standard Room', 100, 300],
            ['Standard Twin Room', 200, 400],
            ['Family Room', 500, 1200],
            ['Deluxe Room', 800, 2000],
        ]);

        $hotels->each(function($hotel) use ($specs, $faker) {
            $specs->filter(function($spec) use ($faker){
                return $faker->boolean;
            })->each(function($spec) use ($hotel, $faker) {
                $price = $faker->numberBetween($spec[1], $spec[2]) * 1000;
                $name = $spec[0];
                $currency = 'IDR';
                $room = new Room(compact('price', 'name', 'currency'));
                $hotel->rooms()->save($room);
            });
        });

        // Sample reviews & votes
        factory(Review::class, 500)->create();
        factory(Vote::class, 2000)->create();
    }
}

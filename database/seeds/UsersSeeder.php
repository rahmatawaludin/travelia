<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 'admin')->create(['email'=>'admin@travelia.com']);
        factory(App\User::class, 'admin', 3)->create();
        factory(App\User::class, 'member')->create(['email'=>'member@travelia.com']);
        factory(App\User::class, 'member', 45)->create();
    }
}

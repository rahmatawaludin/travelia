<?php

Route::get('hotels', 'HotelsController@index');
Route::group(['middleware' => 'auth.basic'], function() {
    Route::post('hotels', 'HotelsController@store');
    Route::get('hotels/{hotel}', 'HotelsController@show');
    Route::match(['patch', 'put'], 'hotels/{hotel}', 'HotelsController@update');
    Route::delete('hotels/{hotel}', 'HotelsController@destroy');
});

// Route::get('hotels/{hotel}/reviews', 'HotelsController@reviews');

Route::group(['prefix' => 'hotels', 'namespace' => 'Hotels'], function() {
    Route::get('{hotel_id}/reviews', 'ReviewsController@index');
});

Route::get('reviews', 'ReviewsController@index');
Route::post('reviews', 'ReviewsController@store');
Route::get('reviews/{review}', 'ReviewsController@show');
